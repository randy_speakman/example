package demo.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class WidgetScheduledJob
{
  private static final Logger LOG = LoggerFactory.getLogger(WidgetScheduledJob.class);

  @Autowired
  private transient JobLauncher jobLauncher;

  @Autowired
  private Job widgetJob;

  @Scheduled(cron = "*/60 * * * * MON-FRI")
  public void execute()
  {
    final JobParametersBuilder builder = new JobParametersBuilder();

    builder.addLong("time", System.currentTimeMillis());
    try
    {
      final JobExecution jobExecution = jobLauncher.run(widgetJob, builder.toJobParameters());
      LOG.debug(String.format("Job [%s] Instance [%s]  Exit code: [%s]", widgetJob.getName(),
          jobExecution.getJobId(), jobExecution.getExitStatus().getExitCode()));
    }
    catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
        | JobParametersInvalidException e)
    {
      LOG.debug("Error running job:" + e);
    }
  }
}
