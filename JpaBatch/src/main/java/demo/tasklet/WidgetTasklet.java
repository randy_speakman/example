package demo.tasklet;

import demo.domain.Widget;
import demo.repositories.WidgetDao;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

public class WidgetTasklet implements Tasklet
{
  private static final Logger LOG = LoggerFactory.getLogger(WidgetTasklet.class);

  @Autowired
  private WidgetDao widgetDao;

  @Override
  public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext)
      throws Exception
  {
    final Widget widget = new Widget();
    widget.setName("Widget" + RandomStringUtils.randomNumeric(3));
    widgetDao.save(widget);
    LOG.debug(String.format("Created Widget.... id:[%s] name:[%s]", widget.getId(), widget.getName()));
    return RepeatStatus.FINISHED;
  }

}
