package demo.jobs;

import demo.config.WidgetBatchConfigurer2;
import demo.tasklet.WidgetTasklet;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WidgetProcessJob
{
  @Autowired
  private StepBuilderFactory steps;

  @Autowired
  private JobBuilderFactory jobs;

  // This bean is commented out because it does not work correctly. This implementation extends
  // DefaultBatchConfigurer which does not appear to work correctly with Spring Boot
  // @Bean
  // public BatchConfigurer batchConfigurer()
  // {
  // return new WidgetBatchConfigurer();
  // }

  /**
   * This bean will override the default spring boot BasicBatchConfigurer.
   * @param dataSource
   * @param entityManagerFactory
   * @return
   */
  @Bean
  public BatchConfigurer batchConfigurer(final DataSource dataSource,
      final EntityManagerFactory entityManagerFactory)
  {
    return new WidgetBatchConfigurer2(dataSource, entityManagerFactory);
  }

  public Step step1()
  {
    return steps.get("STEP1").tasklet(widgetTasklet()).build();
  }

  @Bean
  public Job widgetJob()
  {
    return jobs.get("WidgetJob").start(step1()).build();
  }

  @Bean
  public Tasklet widgetTasklet()
  {
    final WidgetTasklet tasklet = new WidgetTasklet();
    return tasklet;
  }
}
