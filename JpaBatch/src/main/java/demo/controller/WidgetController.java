package demo.controller;

import demo.domain.Widget;
import demo.repositories.WidgetDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/widget")
public class WidgetController
{
  @Autowired
  private WidgetDao widgetDao;

  @RequestMapping(value = "/{id}")
  public Widget getWidget(@PathVariable final Long id)
  {
    return widgetDao.findOne(id);
  }
}
