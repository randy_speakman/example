package demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WIDGET")
public class Widget
{
  private Long id;
  private String name;

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId()
  {
    return id;
  }

  @Column(name = "NAME")
  public String getName()
  {
    return name;
  }

  public void setId(final Long id)
  {
    this.id = id;
  }

  public void setName(final String name)
  {
    this.name = name;
  }

}
