package demo.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.boot.autoconfigure.batch.BasicBatchConfigurer;

/**
 * This class does work correctly with Spring Boot to configure the transaction isolation level
 */
public class WidgetBatchConfigurer2 extends BasicBatchConfigurer
{
  private final DataSource dataSource;

  private EntityManagerFactory entityManagerFactory;

  public WidgetBatchConfigurer2(final DataSource dataSource)
  {
    super(dataSource);
    this.dataSource = dataSource;
  }

  public WidgetBatchConfigurer2(final DataSource dataSource, final EntityManagerFactory entityManagerFactory)
  {
    super(dataSource, entityManagerFactory);
    this.dataSource = dataSource;
    this.entityManagerFactory = entityManagerFactory;
  }

  /**
   * Creates the job repository with ISOLATION_READ_COMMITTED transaction isolation level
   */
  @Override
  protected JobRepository createJobRepository() throws Exception
  {
    final JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
    factory.setDataSource(dataSource);
    if (entityManagerFactory != null)
    {
      factory.setIsolationLevelForCreate("ISOLATION_READ_COMMITTED");
    }
    factory.setTransactionManager(getTransactionManager());
    factory.afterPropertiesSet();
    return factory.getObject();
  }

}
