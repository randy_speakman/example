package demo.config;

import javax.sql.DataSource;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class does not work correctly with Spring Boot but does work in a standalone Java application
 */
public class WidgetBatchConfigurer extends DefaultBatchConfigurer
{
  @Autowired
  private DataSource dataSource;

  /**
   * Sets the transaction isolation level to ISOLATION_READ_COMMITTED
   */
  @Override
  public JobRepository createJobRepository() throws Exception
  {
    final JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
    factory.setDataSource(dataSource);
    factory.setTransactionManager(getTransactionManager());
    factory.setIsolationLevelForCreate("ISOLATION_READ_COMMITTED");
    factory.afterPropertiesSet();
    return factory.getObject();
  }

}
