package demo.repositories;

import demo.domain.Widget;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WidgetDao extends CrudRepository<Widget, Long>
{

}
