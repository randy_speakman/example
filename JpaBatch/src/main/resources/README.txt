1.) The widget job runs once every 60 seconds and creates a Widget entity and saves it to the H2 database.
2.) To test that the entity is saved, in a browser go to the local REST endpoint http://localhost:8080/widget/1
3.) Change the path parameter in the URL to the ID of the Widget you are viewing
4.) Delete the WidgetBatchConfigurer and restart to verify that the JPA entities are saved

--Updated 3/24/2015
5.) If you use the WidgetBatchConfigurer2 class instead, the code works correctly now. This implementation extends the
    org.springframework.boot.autoconfigure.batch.BasicBatchConfigurer class.
