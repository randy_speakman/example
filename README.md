# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Example code for Spring Boot

### How do I get set up? ###

* Clone via GIT
* Import into Eclipse or other IDE
* JPA Batch (Example)
    * Run JpaBatchApplication main method
    * The widget job runs once every 60 seconds and creates a Widget entity and saves it to the H2 database.
    * To test that the entity is saved, in a browser go to the local REST endpoint http://localhost:8080/widget/1
    * Change the path parameter in the URL to the ID of the Widget you are viewing
    * Delete the WidgetBatchConfigurer and restart to verify that the JPA entities are saved